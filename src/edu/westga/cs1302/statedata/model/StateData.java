package edu.westga.cs1302.statedata.model;

import edu.westga.cs1302.statedata.resources.ExceptionMessages;

/**
 * The Class StateData.
 * 
 * The class keeps track of the population data of a state.
 * 
 * @author CS1302
 * @version Fall 2018
 */
public class StateData {

	private String name;
	private int population;
	private int populationChange;

	/**
	 * Instantiates a new account.
	 * 
	 * @precondition name != null && name is not empty AND population >= 0
	 * @postcondition getName() == name AND getPopulation() == population AND
	 *                getPopulationChange() == populationChange
	 *
	 * @param name
	 *            the name
	 * @param population
	 *            the size of the population
	 * @param populationChange
	 *            the change in population size
	 */
	public StateData(String name, int population, int populationChange) {
		if (name == null) {
			throw new IllegalArgumentException(ExceptionMessages.NULL_NAME);
		}
		
		if (name.isEmpty()) {
			throw new IllegalArgumentException(ExceptionMessages.EMPTY_NAME);
		}
		
		if (population < 0) {
			throw new IllegalArgumentException(ExceptionMessages.NEGATIVE_POPULATION);
		}

		this.name = name;
		this.population = population;
		this.populationChange = populationChange;
	}

	/**
	 * Gets the name.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Gets the population.
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the population
	 */
	public int getPopulation() {
		return this.population;
	}

	/**
	 * Sets the population.
	 *
	 * @precondition population >= 0
	 * @postcondition getPopulation() == population
	 *
	 * @param population
	 *            the size of the population
	 */
	public void setPopulation(int population) {
		if (population < 0) {
			throw new IllegalArgumentException(ExceptionMessages.NEGATIVE_POPULATION);
		}
		this.population = population;
	}

	/**
	 * Gets the population change.
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the population change
	 */
	public int getPopulationChange() {
		return this.populationChange;
	}
	
	/**
	 * Sets the population.
	 *
	 * @precondition none
	 * @postcondition getPopulationChange() == populationChange
	 *
	 * @param populationChange
	 *            the change in population
	 */
	public void setPopulationChange(int populationChange) {
		this.populationChange = populationChange;
	}
	
	@Override
	public String toString() {
		return this.name + " : " + this.population;
	}

}
