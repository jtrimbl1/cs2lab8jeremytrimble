package edu.westga.cs1302.statedata.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import edu.westga.cs1302.statedata.resources.ExceptionMessages;

/**
 * The States Class
 * 
 * @author jeremy.trimble
 * @version 11/29/2018
 */
public class States {
	private Map<String, StateData> statesMap;

	/**
	 * Creates a new States Object
	 * 
	 * @precondition none
	 * @postcondition new States Object exist
	 * @param statesMap
	 */
	public States() {
		this.statesMap = new HashMap<String, StateData>();
	}

	/**
	 * Clears the state data from the States Object
	 * 
	 * @precondition none
	 * @postcondition isEmpty() == true
	 * 
	 */
	public void clear() {
		this.statesMap.clear();
	}

	/**
	 * Returns the object that corresponds to the specified key.
	 * 
	 * @precondition key != null
	 * @postcondtion none
	 * 
	 * @param key
	 *            the key of the object to be returned
	 * 
	 * @return the object corresponding to the provided key.
	 */
	public StateData get(Object key) {
		if (key == null) {
			throw new IllegalArgumentException(ExceptionMessages.NULL_KEY);
		}
		return this.statesMap.get(key);
	}

	/**
	 * Returns whether the specified key is contained in the map
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param key
	 *            the key to search for.
	 * 
	 * @return true if the specified key is in the map, else false
	 */
	public boolean containsKey(Object key) {
		return this.statesMap.containsKey(key);
	}

	/**
	 * Return if whether the map is empty or not.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return true is the map is empty else false
	 */
	public boolean isEmpty() {
		return this.statesMap.isEmpty();
	}

	/**
	 * Adds the specified key value pair to the map
	 * 
	 * @precondition key != null
	 * @postcondition size = size@prev + 1
	 * 
	 * @param key
	 *            the key to be added
	 * @param value
	 *            the value to be added
	 * 
	 * @return The previous StateData associated with the key, or null if key had
	 *         previously been specified.
	 */
	public StateData put(String key, StateData value) {
		if (key == null) {
			throw new IllegalArgumentException(ExceptionMessages.NULL_KEY);
		}

		if (this.containsKey(key)) {
			return null;
		}

		return this.statesMap.put(key, value);
	}

	/**
	 * Replaces all the key value pairs with the specified collection
	 * 
	 * @precondition collection != null
	 * @postcondition this.size() == collection.size()
	 * 
	 * @param collection
	 *            the collection to replace the current key value pairs.
	 */
	public void replaceAll(Collection<StateData> collection) {
		if (collection == null) {
			throw new IllegalArgumentException(ExceptionMessages.NULL_COLLECTION);
		}

		this.statesMap.clear();

		for (StateData current : collection) {
			this.statesMap.put(current.getName(), current);
		}
	}

	/**
	 * Removes the specified key
	 * 
	 * @preconditon none
	 * @postcondition this.containsKey(key) == false
	 * 
	 * @param key
	 *            the key to be removed
	 * 
	 * @return the value for the specified key, null if key doesn't exist. Null can
	 *         return can also mean a null value was stored for that key
	 */
	public StateData remove(Object key) {
		return this.statesMap.remove(key);
	}

	/**
	 * Returns the size of the map
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the size of the map
	 */
	public int size() {
		return this.statesMap.size();
	}

	/**
	 * Returns a collection of the values in the map
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return a collection of values in the map
	 */
	public Collection<StateData> values() {
		return this.statesMap.values();

	}
}
