package edu.westga.cs1302.statedata.datatier;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;

import edu.westga.cs1302.statedata.model.StateData;
import edu.westga.cs1302.statedata.resources.ExceptionMessages;

/**
 * The DataReader Class
 * 
 * @author jeremy.trimble
 * @version 12/2/2018
 */
public class DataReader {

	/**
	 * Parses the specified file and returns a collection of stateData contained in
	 * the file
	 * 
	 * @precondition file != null
	 * @postcondition none
	 * 
	 * @param file
	 *            the file containing state data information
	 * 
	 * @return a collection of stateData objects
	 */
	public static Collection<StateData> readStateDataFile(File file) {
		if (file == null) {
			throw new IllegalArgumentException(ExceptionMessages.NULL_FILE);
		}

		Collection<StateData> states = new ArrayList<StateData>();

		try (Scanner scanner = new Scanner(file)) {
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] fields = line.split(",");
				String name = fields[0];
				int population = Integer.parseInt(fields[1]);
				int populationChange = Integer.parseInt(fields[2]);

				StateData state = new StateData(name, population, populationChange);
				states.add(state);
			}
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
		}

		return states;
	}
}
