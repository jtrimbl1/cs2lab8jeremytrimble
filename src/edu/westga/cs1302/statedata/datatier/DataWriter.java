package edu.westga.cs1302.statedata.datatier;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;

import edu.westga.cs1302.statedata.model.StateData;
import edu.westga.cs1302.statedata.resources.ExceptionMessages;

/**
 * The DataWriter Class
 * 
 * @author jeremy.trimble
 * @version 12/2/2018
 */
public class DataWriter {

	private static final String COMMA = ",";

	/**
	 * Writes the specified state data to the specified file
	 * 
	 * @precondition file != null && states != null
	 * @postcondition none
	 * 
	 * @param states
	 *            the stateData objects to be written to the file
	 * @param file
	 *            the file to be written to
	 * 
	 */
	public static void writeStateDataToFile(Collection<StateData> states, File file) {
		if (file == null) {
			throw new IllegalArgumentException(ExceptionMessages.NULL_FILE);
		}
		if (states == null) {
			throw new IllegalArgumentException(ExceptionMessages.NULL_STATES);
		}
		String writerString = "";
		try (FileWriter writer = new FileWriter(file)) {
			for (StateData current : states) {
				writerString += current.getName() + COMMA;
				writerString += current.getPopulation() + COMMA;
				writerString += current.getPopulationChange() + System.lineSeparator();
			}
			writer.append(writerString);
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}

	}
}
