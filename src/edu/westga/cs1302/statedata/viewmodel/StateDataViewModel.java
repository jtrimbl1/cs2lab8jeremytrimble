package edu.westga.cs1302.statedata.viewmodel;

import java.io.File;
import java.util.Collection;

import edu.westga.cs1302.statedata.datatier.DataReader;
import edu.westga.cs1302.statedata.datatier.DataWriter;
import edu.westga.cs1302.statedata.model.StateData;
import edu.westga.cs1302.statedata.model.States;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;

/**
 * The Class SateDataViewModel.
 * 
 * @author CS1302
 * @version Fall 2018
 */
public class StateDataViewModel {

	private final StringProperty stateNameProperty;
	private final IntegerProperty populationProperty;
	private final IntegerProperty populationChangeProperty;
	private final ListProperty<StateData> statesProperty;
	private BooleanProperty dataManagedProperty;
	private States states;

	/**
	 * Instantiates a new password manager gui view model.
	 */
	public StateDataViewModel() {
		this.stateNameProperty = new SimpleStringProperty();
		this.populationProperty = new SimpleIntegerProperty();
		this.populationChangeProperty = new SimpleIntegerProperty();
		this.statesProperty = new SimpleListProperty<StateData>();
		this.states = new States();
		this.dataManagedProperty = new SimpleBooleanProperty(true);
		this.statesProperty.set(FXCollections.observableArrayList(this.states.values()));

	}

	/**
	 * Boolean property that determines if state data is being managed
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the dataManaged property
	 */
	public BooleanProperty dataManagedProperty() {
		return this.dataManagedProperty;
	}

	/**
	 * Gets the state name property.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the state name property
	 */
	public StringProperty stateNameProperty() {
		return this.stateNameProperty;
	}

	/**
	 * Gets the population property.
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the population property
	 */
	public IntegerProperty populationProperty() {
		return this.populationProperty;
	}

	/**
	 * Gets the population change property.
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the population change property
	 */
	public IntegerProperty populationChangeProperty() {
		return this.populationChangeProperty;
	}

	/**
	 * Gets the states property.
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the states property
	 */
	public ListProperty<StateData> statesProperty() {
		return this.statesProperty;
	}

	/**
	 * Adds the state.
	 * 
	 * @precondition none
	 * @postcondition new stateData is added to the listView
	 *
	 * @return true, if successful
	 */
	public boolean addState() {
		String name = this.stateNameProperty.get();
		int population = this.populationProperty.get();
		int change = this.populationChangeProperty.get();
		StateData state = new StateData(name, population, change);
		this.states.put(state.getName(), state);
		this.statesProperty.set(FXCollections.observableArrayList(this.states.values()));
		this.stateNameProperty.set("");
		this.populationProperty.set(0);
		this.populationChangeProperty.set(0);
		this.dataManagedProperty.set(false);
		return true;
	}

	/**
	 * Update state.
	 *
	 * @precondition none
	 * @postcondition State is updated
	 *
	 * @return true, if successful
	 */
	public boolean updateState() {
		StateData state = this.states.get(this.stateNameProperty.get());
		if (state == null) {
			return false;
		}
		int population = this.populationProperty.get();
		int change = this.populationChangeProperty.get();
		state.setPopulation(population);
		state.setPopulationChange(change);
		this.states.put(state.getName(), state);
		this.statesProperty.set(FXCollections.observableArrayList(this.states.values()));
		return true;
	}

	/**
	 * Search for a state.
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @return true, if successful
	 */
	public boolean searchForState() {
		StateData state = this.states.get(this.stateNameProperty.get());
		if (state == null) {
			return false;
		}
		this.populationProperty.set(state.getPopulation());
		this.populationChangeProperty.set(state.getPopulationChange());
		return true;
	}

	/**
	 * Imports the StateData in the specified file
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param file
	 *            the file containing the state data
	 */
	public void importStateData(File file) {
		Collection<StateData> importedData = DataReader.readStateDataFile(file);
		this.states.replaceAll(importedData);
		this.statesProperty.set(FXCollections.observableArrayList(this.states.values()));
		this.dataManagedProperty.set(false);

	}

	/**
	 * Exports the state data into the specified file
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param file
	 *            the file the data is to be saved to
	 */
	public void exportStateData(File file) {
		DataWriter.writeStateDataToFile(this.states.values(), file);

	}

}
