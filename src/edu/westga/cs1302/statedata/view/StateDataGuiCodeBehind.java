package edu.westga.cs1302.statedata.view;

import java.io.File;

import edu.westga.cs1302.statedata.model.StateData;
import edu.westga.cs1302.statedata.viewmodel.StateDataViewModel;
import javafx.beans.binding.BooleanBinding;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.util.converter.NumberStringConverter;

/**
 * The Class StateDataCodeBehind.
 * 
 * @author CS1302
 * @version Lab 8
 */
/**
 * The Class CodeBehind.
 * 
 * @author CS1302
 * @version Fall 2018
 */
public class StateDataGuiCodeBehind {

	private StateDataViewModel viewmodel;

	@FXML
	private AnchorPane guiPane;

	@FXML
	private TextField stateNameTextField;

	@FXML
	private TextField populationTextField;

	@FXML
	private TextField populationChangeTextField;

	@FXML
	private ListView<StateData> statesListView;

	@FXML
	private Button addButton;

	@FXML
	private Button updateButton;

	@FXML
	private Button searchButton;

	@FXML
	private MenuItem fileImportMenuItem;

	@FXML
	private MenuItem fileExportMenuItem;

	/**
	 * Instantiates a new code behind.
	 */
	public StateDataGuiCodeBehind() {
		this.viewmodel = new StateDataViewModel();
	}

	@FXML
	void initialize() {
		this.bindToViewModel();
		this.addListeners();
	}

	private void addListeners() {
		this.setupListenerForItemSelection();
		this.setupBindingForAddButton();
		this.setupBindingForUpdateButton();
		this.setupBindingForSearchButton();

	}

	private void setupBindingForSearchButton() {
		BooleanBinding searchEnabled = this.stateNameTextField.textProperty().isEmpty()
				.or(this.viewmodel.dataManagedProperty());
		this.searchButton.disableProperty().bind(searchEnabled);

	}

	private void setupBindingForUpdateButton() {
		BooleanBinding updateEnabled = this.stateNameTextField.textProperty().isEmpty()
				.or(this.viewmodel.dataManagedProperty());
		this.updateButton.disableProperty().bind(updateEnabled);

	}

	private void setupBindingForAddButton() {
		BooleanBinding addEnabled = this.stateNameTextField.textProperty().isEmpty()
				.or(this.populationTextField.textProperty().isEmpty())
				.or(this.populationChangeTextField.textProperty().isEmpty());
		this.addButton.disableProperty().bind(addEnabled);
	}

	private void setupListenerForItemSelection() {
		this.statesListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			this.stateNameTextField.textProperty().set(newValue.getName());
			Integer population = newValue.getPopulation();
			Integer populationChange = newValue.getPopulationChange();
			this.populationTextField.textProperty().set(population.toString());
			this.populationChangeTextField.textProperty().set(populationChange.toString());
		});
	}

	@FXML
	void handleAddState(ActionEvent event) {
		try {
			if (!this.viewmodel.addState()) {
				this.showAlert("Add error", "ERROR: state was not added");
			}
		} catch (IllegalArgumentException | NullPointerException ex) {
			this.showAlert("Add error", "ERROR: couldn't add the state due to " + ex.getMessage());
		}
	}

	@FXML
	void handleUpdateState(ActionEvent event) {
		try {
			if (!this.viewmodel.updateState()) {
				this.showAlert("Update error", "ERROR: No such a state");
			}
		} catch (IllegalArgumentException | NullPointerException ex) {
			this.showAlert("Update error", "ERROR: couldn't update the state due to " + ex.getMessage());
		}
	}

	@FXML
	void handleSearchForState(ActionEvent event) {
		try {
			if (!this.viewmodel.searchForState()) {
				this.showAlert("Search error", "ERROR: No such state");
			}
		} catch (IllegalArgumentException | NullPointerException ex) {
			this.showAlert("Search error", "ERROR: couldn't find the state due to " + ex.getMessage());
		}
	}

	private void showAlert(String title, String content) {
		Alert alert = new Alert(AlertType.ERROR);
		Window owner = this.guiPane.getScene().getWindow();
		alert.initOwner(owner);
		alert.setTitle(title);
		alert.setContentText(content);
		alert.showAndWait();
	}

	private void bindToViewModel() {
		this.stateNameTextField.textProperty().bindBidirectional(this.viewmodel.stateNameProperty());
		this.populationTextField.textProperty().bindBidirectional(this.viewmodel.populationProperty(),
				new NumberStringConverter());
		this.populationChangeTextField.textProperty().bindBidirectional(this.viewmodel.populationChangeProperty(),
				new NumberStringConverter());
		this.statesListView.itemsProperty().bind(this.viewmodel.statesProperty());
	}

	@FXML
	void exportStateData() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Save File");
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("StateData CSV", "*.csv"),
				new ExtensionFilter("All Files", "*.*"));

		Stage stage = (Stage) this.guiPane.getScene().getWindow();
		File selectedFile = fileChooser.showSaveDialog(stage);
		if (selectedFile != null) {
			this.viewmodel.exportStateData(selectedFile);
		}
	}

	@FXML
	void importStateData() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open File");
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("StateData CSV", "*.csv"),
				new ExtensionFilter("All Files", "*.*"));

		Stage stage = (Stage) this.guiPane.getScene().getWindow();
		File selectedFile = fileChooser.showOpenDialog(stage);
		if (selectedFile != null) {
			this.viewmodel.importStateData(selectedFile);
		}
	}
}